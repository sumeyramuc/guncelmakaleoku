package com.example.sumeyra.xmlparse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.*;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
//import org.w3c.dom.Document;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class SingleMenuItemActivity  extends Activity {

    // XML node keys
    static final String KEY_TITLE = "title";
    static final String KEY_LINK = "link";
    static final String KEY_DESC = "description";
    //static final String KEY_DATE = "pubDate";
    private ProgressDialog progressDialog;
    //private CustomWebViewClient webViewClient;
    private WebView webviev;
    private String url = "";
    ProgressDialog mProgressDialog;
    Button tekrarEdenKelimeler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_list_item);

        tekrarEdenKelimeler=(Button) findViewById(R.id.tekrarEdenKelimeler);
        tekrarEdenKelimeler.setTransformationMethod(null);
        tekrarEdenKelimeler.getBackground().setColorFilter(Color.parseColor("#A52A2A"), PorterDuff.Mode.DARKEN);
        tekrarEdenKelimeler.setTextColor(Color.WHITE);
        tekrarEdenKelimeler.setTextSize(15);

        mProgressDialog = new ProgressDialog(this);//ProgressDialog objesi oluşturuyoruz
        mProgressDialog.setMessage("Yükleniyor...");
        //webViewClient = new CustomWebViewClient();
        // getting intent data
        Intent in = getIntent();
        webviev = (WebView) findViewById(R.id.webView);
        // Get XML values from previous intent
        String title = in.getStringExtra(KEY_TITLE);
        String link = in.getStringExtra(KEY_LINK);
        String description = in.getStringExtra(KEY_DESC);
        url = link;
        //String pubDate = in.getStringExtra(KEY_DATE);

        // Displaying all values on the screen
        TextView lblTitle = (TextView) findViewById(R.id.name_label);
        lblTitle.setTextSize(18);
       // TextView lblLink = (TextView) findViewById(R.id.cost_label);
       // TextView lblDesc = (TextView) findViewById(R.id.description_label);
        // TextView lblDate = (TextView) findViewById(R.id.pubDate);

        lblTitle.setText(title);
        //lblLink.setText(link);
        //lblDesc.setText(description);
        //lblDate.setText(pubDate);
        webviev.getSettings().setJavaScriptEnabled(true);
        webviev.getSettings().setDefaultTextEncodingName("utf-8");
        new Yazar().execute();
        //new getData().execute(link);

    }

    private class Yazar extends AsyncTask<Void,Void,Void>
    {
        String yazar;
        String veri;
        ProgressDialog dialog;
        @Override
        protected  void onPreExecute()
        {
            super.onPreExecute();
            dialog=new ProgressDialog(SingleMenuItemActivity.this);
            //dialog.setTitle("");
            dialog.setMessage("Loading...");
            dialog.setIndeterminate(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Document doc= Jsoup.connect(url).get();
                Elements elements=doc.select("article");
                veri=elements.html();//istenilen html taglarını çeker.

                //yazar=Jsoup.parse(veri).text();//html taglarını texte çevirir.
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void avoid)
        {
            webviev.getSettings().setJavaScriptEnabled(true);
            webviev.getSettings().setDomStorageEnabled(true);
            webviev.setBackgroundColor(Color.TRANSPARENT);
            webviev.loadData(veri, "text/html", "UTF-8");
           // webviev.loadDataWithBaseURL(null, veri, "text/html", "UTF-8", null);
            webviev.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url != null && url.startsWith("http://")) {
                        view.getContext().startActivity(
                                new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            // text.setText(yazar);
            dialog.dismiss();
            tekrarEdenKelimeler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SingleMenuItemActivity.this, kelimesayisi.class);
                    intent.putExtra(Intent.EXTRA_HTML_TEXT, Html.fromHtml(veri));
                    startActivity(intent);
                }
            });
        }
        }

    /*class getData extends AsyncTask<String, String, String> {
        String addline = "";

        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader bufferedReader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    addline += line;
                }
                return addline;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "hatali islem";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        protected void onPostExecute(String s) {
            webviev.getSettings().setBuiltInZoomControls(true); //zoom yapılmasına izin verir
            webviev.getSettings().setSupportZoom(true);
            webviev.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webviev.getSettings().setAllowFileAccess(true);
            webviev.getSettings().setDomStorageEnabled(true);
            webviev.getSettings().setJavaScriptEnabled(true);
            //webviev.setWebViewClient(webViewClient);
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    webviev.loadData(addline, "text/html", "UTF-8");

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    progressDialog.dismiss();

                }
            });

            RequestQueue rQueue = Volley.newRequestQueue(SingleMenuItemActivity.this);
            rQueue.add(request);
        }
    }*/
}

