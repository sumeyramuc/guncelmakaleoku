package com.example.sumeyra.xmlparse;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;

public class  AndroidXMLParsingActivity extends ListActivity {

    // All static variables
    static final String URL = "https://wired.com/feed/";
    // XML node keys
    static final String KEY_ITEM = "item"; // parent node
   // static final String KEY_ID = "id";
    static final String KEY_TITLE = "title";//name
    static final String KEY_LINK = "link";//cost
    static final String KEY_DATE = "pubDate";
    static final String KEY_DESC = "description";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();

        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(URL); // getting XML
        Document doc = parser.getDomElement(xml); // getting DOM element

        NodeList nl = doc.getElementsByTagName(KEY_ITEM);
        // looping through all item nodes <item>
        for (int i = 0; i < 5; i++) {
            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();
            Element e = (Element) nl.item(i);
            // adding each child node to HashMap key => value
            //map.put(KEY_ID, parser.getValue(e, KEY_ID));
            map.put(KEY_TITLE, parser.getValue(e, KEY_TITLE));
            map.put(KEY_LINK, parser.getValue(e, KEY_LINK));
            map.put(KEY_DESC, parser.getValue(e, KEY_DESC));
            map.put(KEY_DATE, parser.getValue(e, KEY_DATE));

            // adding HashList to ArrayList
            menuItems.add(map);
        }

        // Adding menuItems to ListView
        ListAdapter adapter = new SimpleAdapter(this, menuItems,
                R.layout.list_item,
                new String[]{KEY_TITLE, KEY_DESC, KEY_LINK, KEY_DATE}, new int[]{
                R.id.name, R.id.desciption, R.id.cost, R.id.pubDate});

        setListAdapter(adapter);

        // selecting single ListView item
        ListView lv = getListView();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String title = ((TextView) view.findViewById(R.id.name)).getText().toString();
                String link = ((TextView) view.findViewById(R.id.cost)).getText().toString();
                String description = ((TextView) view.findViewById(R.id.desciption)).getText().toString();
                String pubDate = ((TextView) view.findViewById(R.id.pubDate)).getText().toString();

                // Starting new intent
                Intent in = new Intent(getApplicationContext(), SingleMenuItemActivity.class);
                in.putExtra(KEY_TITLE, title);
                in.putExtra(KEY_LINK, link);
                in.putExtra(KEY_DESC, description);
                in.putExtra(KEY_DATE, pubDate);
                startActivity(in);

            }
        });
    }
}

