
### What is this repository for? ###

Güncel Makale Okuma Projesi Wired.com adresinden son yayınlanan 5 makaleyi ekranda listeler.
Seçilen makalenin içeriğine ulaşabilen kullanıcı ayrıca, bu makalede en sık tekrarlayan 5 kelimenin Türkçe karşılığını da 
görebilmektedir.
Bu uygulama Android Studio ortamında Xml datalar, Web service(Restful api), Http Protokolleri ve HtmlParser kütüphaneleri(Jsoup)
kullanılarak geliştirlmiştir.
>>Ayrıca bu uygulamanın Json kullanılarak yapılan bir uygulaması da mevcuttur.